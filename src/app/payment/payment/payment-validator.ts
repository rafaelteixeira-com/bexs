export class PaymentValidator {

    static creditCardValidator(control) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if ( PaymentValidator.checkCardNumber(control.value) ) {
            return null;
        } else {
            return { invalidCreditCard: true };
        }
    }

    static nameValidator(control) {
        if ( control.value.match( /^[a-zA-Z]{2,18}\s[a-zA-Z]{1,18}(\s[a-zA-Z]{1,18})?(\s[a-zA-Z]{1,18})?(\s[a-zA-Z]{1,18})?\s?$/ ) ) { 
            return null;
        } else {
            return { invalidName: true };
        }
    }

    static dateValidator(control) {
        let val = control.value;
        console.log(' DATE val: ', val);
        let month = parseInt( val.substr(0, 2) );
        let year = parseInt( val.substr(2, 2) );
        if(month > 12 || month < 1) return { invalidDate: true };
        year += 2000;
        if(!month || !year ) return null;
        let date = new Date(year, month);
        if ( new Date().getTime() < date.getTime()) { 
            return null;
        } else {
            return { invalidDate: true };
        }
    }

    static checkCardNumber(num){
        let total;
        num = num.replace( /\s/g, '')

        //console.log(num);

        if(num.length > 16 || num[0]==0){
            
            return false;
            
        } else {
            
            total = 0;
            let arr = Array();
            
            for(let i=0;i<num.length;i++){
                if(i%2==0){
                    let dig:any = num[i] * 2;
                    
                    if(dig > 9){
                        let dig1 = dig.toString().substr(0,1);
                        let dig2 = dig.toString().substr(1,1);
                        arr[i] = parseInt(dig1)+parseInt(dig2);
                    } else {
                        arr[i] = parseInt(dig);
                    }
                        
                    total += parseInt(arr[i]);

                } else {

                    arr[i] =parseInt(num[i]);
                    total += parseInt(arr[i]);
                } 
            }

            if(total%10 !== 0){
                return false
            }
        }
        return true;
    }

    static getCardType(num) {
        let cardType = {
            type : null,
            operator : null
        };

        console.log(num);

        switch( parseInt(num[0]) ){
            case 0:
                return cardType;
                break;
            case 1:
                cardType.type = "Empresas Aéreas";
                break;
            case 2:
                cardType.type = "Empresas Aéreas";
                break
            case 3:
                cardType.type = "Viagens e Entretenimento";
                if(parseInt(num[0]+num[1]) == 34 || parseInt(num[0]+num[1])==37){ cardType.operator = "Amex";	} 
                if(parseInt(num[0]+num[1]) == 36){	cardType.operator = "Diners";	} 
                break
            case 4:
                cardType.type = "Bancos e Instituições Financeiras";
                cardType.operator = "visa";
                break
            case 5:
                if(parseInt(num[0]+num[1]) >= 51 && parseInt(num[0]+num[1])<=55){	cardType.operator = "Mastercard";	} 
                cardType.type = "Bancos e Instituições Financeiras";
                cardType.operator = "Mastercard"
                break;
            case 6:
                cardType.type = "Bancos e Comerciais";
                cardType.operator = "";
                break
            case 7:
                cardType.type = "Companhias de petróleo";
                cardType.operator = "";
                break
            case 8:
                cardType.type = "Companhia de telecomunicações";
                cardType.operator = "";
                break
            case 9:
                cardType.type = "Nacionais";
                cardType.operator = "";
                break
            default:
                break;
        }
        console.log('getCardType', cardType);
        return cardType;
    }
}