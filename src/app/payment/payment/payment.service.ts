import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { API_SERVER } from './../../app.api';



@Injectable({ providedIn: 'root' })
export class PaymentService {

    constructor(private http: HttpClient) {
    }

    // end-point para calcular a progressão do CDB de acordo com a proposta do teste
    payCard(paymentData) {
        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
            })
          };
          return this.http.post<any>(`${API_SERVER}/payment-fake`, paymentData, httpOptions)
              .pipe(map(result => {
                  return result;
              }));
    }

}