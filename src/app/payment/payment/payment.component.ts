import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';

import { PaymentValidator } from './payment-validator';

import { PaymentService } from './payment.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.sass']
})
export class PaymentComponent implements OnInit {

  stepActive:String = 'card';

  formPayment: FormGroup;

  msgError:String;
  showResult:Boolean;
  isLoading:Boolean;

  contextForm = {
    showBack:false,
    operatorCard: '',
    numberCard: '**** **** **** ****',
    numberCvv: '***',
    nameCard: 'NOME DO TÍTULAR',
    dateCard: '00/00',
    installment:[
      {value: 1, label:'1x R$12.000,00 sem juros'},
      {value: 2, label:'2x R$6.000,00 sem juros'},
      {value: 3, label:'3x R$4.000,00 sem juros'},
      {value: 4, label:'4x R$3.000,00 sem juros'},
      {value: 5, label:'5x R$2.400,00 sem juros'},
      {value: 6, label:'6x R$2.000,00 sem juros'},
      {value: 7, label:'7x R$1.714,28 sem juros'},
      {value: 8, label:'8x R$1.500,00 sem juros'},
      {value: 9, label:'9x R$1.333,33 sem juros'},
      {value: 10, label:'10x R$1.200,00 sem juros'},
      {value: 11, label:'11x R$1.090,90 sem juros'},
      {value: 12, label:'12x R$1.000,00 sem juros'},
    ]
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private paymentService: PaymentService
  ) {
    this.initContextForm();
  }

  ngOnInit(): void {
  }

  // inicia o contexto (campos) do formulário
  initContextForm(){
    this.formPayment = this.formBuilder.group({
      number : ['', [Validators.required,  PaymentValidator.creditCardValidator] ],
      name   : ['', [Validators.required, PaymentValidator.nameValidator] ],
      date   : ['', [Validators.required, Validators.minLength(4), PaymentValidator.dateValidator] ],
      cvv    : ['', [Validators.required, Validators.minLength(3)] ],
      installment  : ['', [Validators.required, Validators.minLength(1)] ]
    });
  }

  getRouterActive(){
    let url = this.router.url;
    console.log('url', url);
  }

  changeTypeCard($event){
    let typeCard = PaymentValidator.getCardType($event.target.value);
    this.contextForm.operatorCard = typeCard.operator;
  }


  // esconde o resultado enquanto manipular o formulário
  hideResult(){
    this.showResult = false;
  }
  
  get f() { return this.formPayment.controls; }

  continue() {
    
    this.msgError = null;
    this.hideResult();
    if (this.formPayment.invalid) {
      return;
    }

    this.isLoading = true;
    this.showResult = false;

    const payData = {
      number: this.f.number.value,
      name: this.f.name.value,
      date: this.f.date.value,
      cvv: this.f.cvv.value,
      installment: this.f.installment.value
    }

    this.paymentService.payCard(payData).subscribe(  
      content =>{
        this.isLoading = false;
        this.showResult = true;
      },
      error => {
        this.isLoading = false;
        this.showResult = false;
        this.msgError = 'Ocorreu um erro interno.';
        console.error('Ocorreu um erro no serviço: ', error)
      }
    );
  }

}
