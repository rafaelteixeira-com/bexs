import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart/cart.component';
import { CardComponent } from './card/card.component';
import { FinishComponent } from './finish/finish.component';
import { RouterModule } from '@angular/router';
import { routes } from './payment-routing.module';
import { PaymentComponent } from './payment/payment.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { NgxMaskModule } from 'ngx-mask';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SubloaderComponent } from './../subloader/subloader.component';
@NgModule({
  declarations: [SubloaderComponent, CartComponent, CardComponent, FinishComponent, PaymentComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild(routes)
  ]
})
export class PaymentModule {

  constructor() {

  }

 }
